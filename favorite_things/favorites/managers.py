from django.db import models, transaction
from django.db.models import F

from favorite_things.utils.simple_history import bulk_update_with_history


class ThingManager(models.Manager):
    def reorder_rank(
        self, category_id, ranking, pk=None, increment=False, decrement=False
    ):
        """This method enables sorting of ranks affected by create, update or delete

        :param category_id: Category object id
        :param ranking: A numbered priority (1...n)
        :param pk: Model object id
        :param increment: Reordering of rank by increasing its value
        :param decrement: Reordering of rank by decreasing its value
        :return:
        """
        if increment == decrement:
            raise ValueError("Set either increment or decrement to True only")

        value = 1 if increment else -1
        queryset = self.model.objects.filter(
            category_id=category_id, ranking__gte=ranking
        )
        if pk:
            queryset = queryset.exclude(pk=pk)

        if queryset.count():
            bulk_update_with_history(queryset, {"ranking": F("ranking") + value})

    @transaction.atomic()
    def create_one(self, **kwargs):
        """Method to create a single ``Thing`` object"""
        self.reorder_rank(kwargs["category"].id, kwargs["ranking"], increment=True)
        return super().create(**kwargs)

    @transaction.atomic()
    def update_one(self, instance, update_rank=True):
        """Method to update a single ``Thing`` object"""
        if update_rank:
            self.reorder_rank(
                instance.category_id, instance.ranking, pk=instance.pk, increment=True
            )
        instance.save()
        return instance

    @transaction.atomic()
    def delete_one(self, instance):
        """Method to delete a single ``Thing`` object"""
        category_id = instance.category_id
        ranking = instance.ranking
        instance.delete()
        self.reorder_rank(category_id, ranking, decrement=True)
