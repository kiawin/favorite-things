import pytest

from favorite_things.favorites.tests.factories import CategoryFactory, ThingFactory


@pytest.fixture
def create_category():
    def fn(**kwargs):
        return CategoryFactory(**kwargs)

    return fn


@pytest.fixture
def create_categories():
    def fn(quantity=1, **kwargs):
        return CategoryFactory.create_batch(quantity, **kwargs)

    return fn


@pytest.fixture
def create_thing():
    def fn(**kwargs):
        return ThingFactory(**kwargs)

    return fn


@pytest.fixture
def create_things():
    def fn(quantity=1, **kwargs):
        return ThingFactory.create_batch(quantity, **kwargs)

    return fn
