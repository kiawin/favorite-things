from simple_history.utils import get_history_manager_for_model


def bulk_update_with_history(queryset, kwargs, batch_size=None):
    """A method to provide a utility to update in bulk and record history

    This method is based on ``bulk_create_with_history()`` taken from ``django-simple-history/simple_history/utils.py``

    :param queryset: The queryset used to perform bulk update
    :param kwargs: The fields to be updated
    :param batch_size: Number of objects that should be created in each batch
    :return:
    """
    result = queryset.update(**kwargs)
    objs_with_id = list(queryset.all())
    history_manager = get_history_manager_for_model(queryset.model)
    history_manager.bulk_history_create(objs_with_id, batch_size=batch_size)

    return result
