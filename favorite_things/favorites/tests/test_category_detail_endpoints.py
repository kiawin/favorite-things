import json

import pytest
from faker import Faker
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

pytestmark = pytest.mark.django_db
fake = Faker()


class TestCategoryDetailEndpoints:
    def test_get(self, create_category):
        """Retrieve category detail"""
        item = create_category()

        api = APIClient()
        response = api.get(reverse("category-detail", kwargs={"pk": item.pk}))

        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        assert contents["id"] == item.id

    def test_patch(self, create_category):
        """Update category detail"""
        item = create_category()
        new_name = fake.name()
        data = json.dumps({"name": new_name})

        api = APIClient()
        response = api.patch(
            path=reverse("category-detail", kwargs={"pk": item.pk}),
            data=data,
            content_type="application/json",
        )

        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        assert contents["name"] == new_name

    def test_post(self, create_category):
        """Update category detail"""
        item = create_category()
        new_name = fake.name()
        data = json.dumps({"id": item.id, "name": new_name})

        api = APIClient()
        response = api.post(
            path=reverse("category-detail", kwargs={"pk": item.pk}),
            data=data,
            content_type="application/json",
        )

        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_delete(self, create_category):
        item = create_category()

        api = APIClient()
        response = api.delete(path=reverse("category-detail", kwargs={"pk": item.pk}))
        assert response.status_code == status.HTTP_204_NO_CONTENT
