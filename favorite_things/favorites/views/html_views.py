from django.shortcuts import redirect
from rest_framework.generics import get_object_or_404
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from favorite_things.favorites.models import Category, Thing
from favorite_things.favorites.serializers import (
    CategorySerializer,
    ThingSerializer,
    ThingHistorySerializer,
)


class CategoryListView(APIView):
    schema = None
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "../templates/pages/category.html"

    def get(self, request):
        queryset = Category.objects.all()
        return Response({"data": queryset})


class CategoryAddView(APIView):
    schema = None
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "../templates/pages/category_add.html"

    def get(self, request):
        serializer = CategorySerializer()
        return Response({"serializer": serializer})

    def post(self, request):
        serializer = CategorySerializer(data=request.data)
        if not serializer.is_valid():
            return Response({"serializer": serializer})
        serializer.save()
        return redirect("html-category")


class CategoryDeleteView(APIView):
    schema = None
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "../templates/pages/category_delete.html"

    def post(self, request, pk):
        category = get_object_or_404(Category, pk=pk)
        category.delete()
        return redirect("html-category")


class CategoryDetailView(APIView):
    schema = None
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "../templates/pages/category_detail.html"

    def get(self, request, pk):
        category = get_object_or_404(Category, pk=pk)
        serializer = CategorySerializer(category)
        return Response({"serializer": serializer, "category": category})

    def post(self, request, pk):
        category = get_object_or_404(Category, pk=pk)
        serializer = CategorySerializer(category, data=request.data)
        if not serializer.is_valid():
            return Response({"serializer": serializer, "category": category})
        serializer.save()
        return redirect("html-category")


class ThingListView(APIView):
    schema = None
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "../templates/pages/thing.html"

    def get(self, request):
        queryset = Thing.objects.all()
        return Response({"data": queryset})


class ThingAddView(APIView):
    schema = None
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "../templates/pages/thing_add.html"

    def get(self, request):
        serializer = ThingSerializer()
        return Response({"serializer": serializer})

    def post(self, request):
        serializer = ThingSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({"serializer": serializer})
        serializer.save()
        return redirect("html-thing")


class ThingDeleteView(APIView):
    schema = None
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "../templates/pages/thing_delete.html"

    def post(self, request, pk):
        thing = get_object_or_404(Thing, pk=pk)
        Thing.objects.delete_one(thing)
        return redirect("html-thing")


class ThingDetailView(APIView):
    schema = None
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "../templates/pages/thing_detail.html"

    def get(self, request, pk):
        thing = get_object_or_404(Thing, pk=pk)
        serializer = ThingSerializer(thing)
        return Response({"serializer": serializer, "thing": thing})


class ThingDetailHistoryView(APIView):
    schema = None
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "../templates/pages/thing_detail_history.html"

    def get(self, request, pk):
        thing = get_object_or_404(Thing, pk=pk)
        serializer = ThingHistorySerializer(thing)
        return Response({"serializer": serializer, "thing": thing})


class ThingDetailEditView(APIView):
    schema = None
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "../templates/pages/thing_detail_edit.html"

    def get(self, request, pk):
        thing = get_object_or_404(Thing, pk=pk)
        serializer = ThingSerializer(thing)
        return Response({"serializer": serializer, "thing": thing})

    def post(self, request, pk):
        thing = get_object_or_404(Thing, pk=pk)
        serializer = ThingSerializer(thing, data=request.data)
        if not serializer.is_valid():
            return Response({"serializer": serializer, "thing": thing})
        serializer.save()
        return redirect("html-thing")
