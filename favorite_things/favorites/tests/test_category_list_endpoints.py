import json

import pytest
from faker import Faker
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

pytestmark = pytest.mark.django_db
fake = Faker()


class TestCategoryListEndpoints:
    def test_get(self, create_category):
        """Retrieve category list"""
        item = create_category()

        api = APIClient()
        response = api.get(reverse("category-list"))

        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        # There are 3 predefined categories in ``favorite_things/favorites/migrations/0001_initial.py``
        assert len(contents) == 3 + 1
        assert contents[-1]["id"] == item.id

    @pytest.mark.parametrize(
        "name",
        (
            fake.name(),  # pylint: disable=no-member
            # name cannot be empty
            pytest.param("", marks=pytest.mark.xfail),
        ),
    )
    def test_post(self, name):
        """Create new category"""
        data = json.dumps({"name": name})

        api = APIClient()
        response = api.post(
            path=reverse("category-list"), data=data, content_type="application/json"
        )
        assert response.status_code == status.HTTP_201_CREATED
