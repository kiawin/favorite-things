from json_tricks import loads, dumps
from rest_framework import serializers

from favorite_things.favorites.models import Category, Thing
from favorite_things.utils.json import validate_metadata_json


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ("id", "name", "created_date", "modified_date")
        read_only_fields = ("created_date", "modified_date")


class ThingSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        # Defaulted ``null`` to ``{}`` for metadata field
        if validated_data.get("metadata") is None:
            validated_data["metadata"] = {}
        instance = Thing.objects.create_one(**validated_data)
        return instance

    def update(self, instance, validated_data):
        update_rank = True if 'ranking' in validated_data and instance.ranking != validated_data['ranking'] else False
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        return Thing.objects.update_one(instance, update_rank=update_rank)

    def validate_metadata(self, metadata):
        # Skip validation as metadata with ``null`` will be defaulted to ``{}`` during creation
        # `As `null`` is also a valid value for JSON, we allow this value during update
        if metadata is None:
            return metadata

        try:
            # Ensure the metadata is loadable by json-tricks
            # - ``dumps`` action is performed as we need to first convert the dict
            #   to json string prior converting the values back to python object.
            json_string = loads(dumps(metadata))
            validate_metadata_json(json_string)
        except Exception:  # pylint: disable=broad-except
            raise serializers.ValidationError("Invalid metadata content.")

        return metadata

    class Meta:
        model = Thing
        fields = (
            "id",
            "title",
            "description",
            "ranking",
            "metadata",
            "category",
            "created_date",
            "modified_date",
        )
        read_only_fields = ("created_date", "modified_date")


class ThingHistorySerializer(ThingSerializer):
    history = serializers.SerializerMethodField()

    class Meta:
        model = Thing
        fields = ("history", *ThingSerializer.Meta.fields)
        read_only_fields = ("history", *ThingSerializer.Meta.read_only_fields)

    def get_history(self, instance):
        records = instance.history.order_by("history_date")
        data = []
        old_record = records[0]
        for version, new_record in enumerate(records[1:], start=1):
            delta = new_record.diff_against(old_record)
            for change in delta.changes:
                data.append(
                    {
                        "version": version,
                        "field": Thing._meta.get_field(change.field).verbose_name,
                        "old": change.old,
                        "new": change.new,
                    }
                )
            old_record = new_record
        return data
