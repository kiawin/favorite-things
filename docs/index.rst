.. Favorite Things documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Favorite Things Project Documentation
====================================================================

Table of Contents:

.. toctree::
   :maxdepth: 2


Indices & Tables
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
