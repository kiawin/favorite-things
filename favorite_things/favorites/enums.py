from enum import Enum


class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3


class Status(Enum):
    DONE = 1
    TODO = 2
    WISHLIST = 3
