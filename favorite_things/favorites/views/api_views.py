from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from favorite_things.favorites.models import Category, Thing
from favorite_things.favorites.serializers import (
    CategorySerializer,
    ThingSerializer,
    ThingHistorySerializer,
)


class CategoryViewSet(viewsets.ModelViewSet):
    """
    retrieve:
    Return the given category.

    list:
    Return a list of all the existing categories.

    create:
    Create a new category instance.
    """

    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ThingViewSet(viewsets.ModelViewSet):
    """View for Thing
    retrieve:
    Return the given thing.

    list:
    Return a list of all the existing things.

    create:
    Create a new thing instance.
    """

    queryset = Thing.objects.all()
    serializer_class = ThingSerializer

    def perform_destroy(self, instance):
        """Allow the view to delete single instance only"""
        Thing.objects.delete_one(instance)

    @action(detail=True)
    def history(self, request, pk=None):
        """
        get:
        Return history of the given thing.
        """
        thing = self.get_object()
        serializer = ThingHistorySerializer(thing)
        return Response({"history": serializer.data["history"]})
