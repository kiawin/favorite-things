from factory import DjangoModelFactory, Faker, SubFactory

from favorite_things.favorites.models import Category, Thing


class CategoryFactory(DjangoModelFactory):
    name = Faker("name")

    class Meta:
        model = Category
        django_get_or_create = ("name",)


class ThingFactory(DjangoModelFactory):

    title = Faker("name")
    category = SubFactory(CategoryFactory)
    description = Faker("text")
    metadata = {}
    ranking = 1

    class Meta:
        model = Thing
        django_get_or_create = ("title",)
