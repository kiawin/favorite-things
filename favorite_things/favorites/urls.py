from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from favorite_things.favorites.views.api_views import CategoryViewSet, ThingViewSet

# Setup router to determine URL configuration
router = routers.DefaultRouter()
router.register(r"categories", CategoryViewSet)
router.register(r"things", ThingViewSet)

# Setup API using automatic URL routing
urlpatterns = [url(r"^", include(router.urls))]
