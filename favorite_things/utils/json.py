from collections.abc import Mapping


class InvalidMetadataException(Exception):
    pass


def validate_metadata_json(data):
    """Validate metadata JSON structure

    :param data: metadata dictionary
    :return:
    :raises: InvalidMetadataException: raised when invalid metadata structure is found
    """
    for value in data.values():
        # If the value is a dictionary, it has to be either enum or date type.
        if isinstance(value, Mapping):
            if "__date__" not in value and "__enum__" not in value:
                raise InvalidMetadataException()
