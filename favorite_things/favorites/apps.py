from django.apps import AppConfig


class FavoritesConfig(AppConfig):
    name = "favorite_things.favorites"
    verbose_name = "Favorites"
