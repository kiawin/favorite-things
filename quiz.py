from cryptography.fernet import Fernet

key = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='

# Oh no! The code is going over the edge! What are you going to do?
message = b'gAAAAABdIA3BpwZ5RFXBdVGA7C7ZH9GoUovK22Jun8JuS9J0H0OlgY_qhXXwTM4xuycQRzuLOuoByvvlB-fFKI952FuIxOiivIb98VihCpzI8k-4XLk0zGbS0OFKXVIXUkX3KamnHakocEn2qx3O_ecI5NbVJuSLt6SSIl2e6KQwO59gS0Qc-Hg='


def main():
    f = Fernet(key)
    print(f.decrypt(message))


if __name__ == "__main__":
    main()
