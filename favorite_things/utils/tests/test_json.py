import pytest
from json_tricks import loads

from favorite_things.utils.json import validate_metadata_json, InvalidMetadataException


def test_valid_metadata():
    data = loads(
        """
    {
      "text_example": "abcdef",
      "number_example": 12.34,
      "date_example": {
        "__date__": null,
        "year": 2019,
        "month": 7,
        "day": 20
      },
      "enum_example": {
        "__enum__": {
          "__enum_instance_type__": ["favorite_things.favorites.enums", "Status"],
          "name": "DONE"
        }
      }
    }
    """
    )
    validate_metadata_json(data)


def test_metadata_with_invalid_key():
    data = loads(
        """
    {
      "date_example": {
        "__date1234__": null,
        "year": 2019,
        "month": 7,
        "day": 20
      }
    }
    """
    )
    with pytest.raises(InvalidMetadataException):
        validate_metadata_json(data)
