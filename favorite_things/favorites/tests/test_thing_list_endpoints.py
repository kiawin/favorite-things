import json

import pytest
from faker import Faker
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

pytestmark = pytest.mark.django_db
fake = Faker()


class TestThingListEndpoints:
    def test_get(self, create_thing):
        """Retrieve thing list"""
        item = create_thing()

        api = APIClient()
        response = api.get(reverse("thing-list"))

        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        assert len(contents) == 1
        assert contents[0]["id"] == item.id

    @pytest.mark.parametrize(
        "title,description",
        (
            (fake.name(), fake.pystr(min_chars=10)),
            (fake.name(), ""),
            # title cannot be empty
            pytest.param("", fake.pystr(min_chars=10), marks=pytest.mark.xfail),
            # description is either empty or a minimum length of 10
            pytest.param(
                fake.name(), fake.text(max_nb_chars=9), marks=pytest.mark.xfail
            ),
        ),
    )
    def test_post(self, create_category, title, description):
        """Creating new thing"""
        category = create_category()
        data = json.dumps(
            {
                "title": title,
                "description": description,
                "category": category.id,
                "ranking": 1,
            }
        )

        api = APIClient()
        response = api.post(
            path=reverse("thing-list"), data=data, content_type="application/json"
        )
        assert response.status_code == status.HTTP_201_CREATED

    def test_reorder(self, create_category, create_thing):
        category = create_category()
        thing1 = create_thing(  # pylint: disable=unused-variable
            ranking=1, category=category
        )
        thing2 = create_thing(ranking=2, category=category)
        data = json.dumps({"title": fake.name(), "ranking": 2, "category": category.id})

        api = APIClient()
        response = api.post(
            path=reverse("thing-list"), data=data, content_type="application/json"
        )
        assert response.status_code == status.HTTP_201_CREATED

        api = APIClient()
        response = api.get(reverse("thing-detail", kwargs={"pk": thing2.pk}))
        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        # Verify the ranking has been amended
        assert contents["ranking"] == thing2.ranking + 1
