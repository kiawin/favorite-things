import json

import pytest
from faker import Faker
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

pytestmark = pytest.mark.django_db
fake = Faker()


class TestThingDetailEndpoints:
    def test_get(self, create_thing):
        """Retrieve thing detail"""
        item = create_thing()

        api = APIClient()
        response = api.get(reverse("thing-detail", kwargs={"pk": item.pk}))

        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        assert contents["id"] == item.id

    def test_patch(self, create_thing):
        """Update thing detail"""
        item = create_thing()
        new_title = fake.name()
        data = json.dumps({"title": new_title})

        api = APIClient()
        response = api.patch(
            path=reverse("thing-detail", kwargs={"pk": item.pk}),
            data=data,
            content_type="application/json",
        )

        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        assert contents["title"] == new_title

    def test_post(self, create_thing):
        """Update thing detail"""
        item = create_thing()
        new_title = fake.name()
        data = json.dumps(
            {
                "id": item.id,
                "title": new_title,
                "description": item.description,
                "metadata": item.metadata,
                "ranking": item.ranking,
            }
        )

        api = APIClient()
        response = api.post(
            path=reverse("thing-detail", kwargs={"pk": item.pk}),
            data=data,
            content_type="application/json",
        )

        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_delete(self, create_thing):
        item = create_thing()

        api = APIClient()
        response = api.delete(path=reverse("thing-detail", kwargs={"pk": item.pk}))
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_reorder(self, create_category, create_thing):
        category = create_category()
        thing1 = create_thing(  # pylint: disable=unused-variable
            ranking=1, category=category
        )
        thing2 = create_thing(ranking=2, category=category)
        thing3 = create_thing(ranking=3, category=category)

        new_ranking = 2
        data = json.dumps({"ranking": new_ranking})

        api = APIClient()
        response = api.patch(
            path=reverse("thing-detail", kwargs={"pk": thing3.pk}),
            data=data,
            content_type="application/json",
        )
        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        # Verify the ranking has been amended
        assert contents["ranking"] == new_ranking

        api = APIClient()
        response = api.get(reverse("thing-detail", kwargs={"pk": thing2.pk}))
        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        # Verify the ranking has been amended
        assert contents["ranking"] == thing2.ranking + 1

    def test_history(self, create_thing):
        thing = create_thing()

        update_count = 5
        api = APIClient()
        for title in [fake.name() for i in range(update_count)]:
            data = json.dumps({"title": title})
            response = api.patch(
                path=reverse("thing-detail", kwargs={"pk": thing.pk}),
                data=data,
                content_type="application/json",
            )
            assert response.status_code == status.HTTP_200_OK
        path = f"{reverse('thing-detail', kwargs={'pk': thing.pk})}history/"
        response = api.get(path)
        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        # Verify the history records is 2 times of ``update_count``
        # owing to field changes applicable to ``title`` and ``modified_date``
        assert len(contents["history"]) == update_count * 2

    @pytest.mark.parametrize(
        "metadata",
        (
            # metadata can be empty object
            {},
            # metadata can be ``null``
            None,
            # valid types (enum, date)
            {
                "enum": {
                    "__enum__": {
                        "__enum_instance_type__": [
                            "favorite_things.favorites.enums",
                            "Status",
                        ],
                        "name": "DONE",
                    }
                }
            },
            {"date": {"__date__": None, "year": 1, "month": 1, "day": 1}},
            # metadata cannot be empty string
            pytest.param("", marks=pytest.mark.xfail),
            # metadata cannot allow invalid type
            pytest.param(
                {"invalid_type": {"___123___": None}}, marks=pytest.mark.xfail
            ),
            # metadata cannot allow incomplete date type
            pytest.param(
                {"incomplete_date": {"__date__": None}}, marks=pytest.mark.xfail
            ),
            # metadata cannot allow invalid enum value
            pytest.param(
                {
                    "incomeplete_enum": {
                        "__enum__": {
                            "__enum_instance_type__": [
                                "favorite_things.favorites.enums",
                                "Status",
                            ],
                            "name": "INVALID_ENUM",
                        }
                    }
                },
                marks=pytest.mark.xfail,
            ),
        ),
    )
    def test_patch_metadata(self, create_thing, metadata):
        """Update thing detail"""
        item = create_thing()
        data = json.dumps({"metadata": metadata})

        api = APIClient()
        response = api.patch(
            path=reverse("thing-detail", kwargs={"pk": item.pk}),
            data=data,
            content_type="application/json",
        )

        assert response.status_code == status.HTTP_200_OK
        contents = response.json()
        assert contents["metadata"] == metadata
