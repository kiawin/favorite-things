from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

from favorite_things.favorites.models import Category, Thing


@admin.register(Category)
class CategoryAdmin(SimpleHistoryAdmin):
    pass


@admin.register(Thing)
class ThingAdmin(SimpleHistoryAdmin):
    pass
