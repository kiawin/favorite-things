from django.conf import settings
from django.urls import include, path, re_path
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views
from django.views.generic import TemplateView
from rest_framework.documentation import include_docs_urls

from favorite_things.favorites.views.html_views import (
    CategoryListView, ThingListView, CategoryDetailView, CategoryAddView,
    CategoryDeleteView, ThingAddView, ThingDeleteView, ThingDetailView,
    ThingDetailEditView, ThingDetailHistoryView)

urlpatterns = [
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    # Frontend views
    path("", ThingListView.as_view(), name="html-thing"),
    path("category/", CategoryListView.as_view(), name="html-category"),
    path("category/add", CategoryAddView.as_view(), name="html-category-add"),
    re_path(r"^category/(?P<pk>\d+)/delete$", CategoryDeleteView.as_view(), name="html-category-delete"),
    re_path(r"^category/(?P<pk>\d+)/$", CategoryDetailView.as_view(), name="html-category-detail"),
    path("thing/add", ThingAddView.as_view(), name="html-thing-add"),
    re_path(r"^thing/(?P<pk>\d+)/delete$", ThingDeleteView.as_view(), name="html-thing-delete"),
    re_path(r"^thing/(?P<pk>\d+)/edit$", ThingDetailEditView.as_view(), name="html-thing-detail-edit"),
    re_path(r"^thing/(?P<pk>\d+)/history$", ThingDetailHistoryView.as_view(), name="html-thing-detail-history"),
    re_path(r"^thing/(?P<pk>\d+)/$", ThingDetailView.as_view(), name="html-thing-detail"),
    # Help
    path('help/metadata', TemplateView.as_view(template_name="pages/metadata.html"), name="help-metadata"),
    # API views
    path("favorites/", include("favorite_things.favorites.urls")),
    path('docs/', include_docs_urls(title='Favorite Things API')),
    # Timezone
    re_path(r'^tz_detect/', include('tz_detect.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
