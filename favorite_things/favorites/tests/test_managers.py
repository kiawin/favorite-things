import pytest
from factory import Faker

from favorite_things.favorites.models import Thing

pytestmark = pytest.mark.django_db


class TestThingManager:
    @pytest.mark.parametrize("ranking,new_thing_ranking", [(1, 2), (2, 1)])
    def test_create_one(
        self, create_thing, create_category, ranking, new_thing_ranking
    ):
        category = create_category()
        thing = create_thing(category=category, ranking=1)

        new_thing = Thing.objects.create_one(
            title=Faker("name"),
            description=Faker("text"),
            ranking=ranking,
            category=thing.category,
        )
        assert new_thing.ranking == ranking
        assert Thing.objects.get(pk=thing.pk).ranking == new_thing_ranking
