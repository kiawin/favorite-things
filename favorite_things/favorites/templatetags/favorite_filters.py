from datetime import datetime

from dateutil.parser import parse
from django import template
from django.utils.safestring import mark_safe
from json_tricks import dumps


register = template.Library()


@register.filter(name="json")
def json_dumps(data):
    return mark_safe(dumps(data, indent=2))


@register.filter(name="dt")
def to_datetime(value):
    return parse(value)
