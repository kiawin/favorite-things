from django.contrib.postgres.fields import JSONField
from django.core.validators import MinLengthValidator
from django.db import models
from django.urls import reverse_lazy
from django.utils.text import format_lazy
from django.utils.translation import ugettext_lazy as _
from simple_history.models import HistoricalRecords

from favorite_things.favorites.managers import ThingManager


class Category(models.Model):
    name = models.CharField(
        verbose_name=_("Name"),
        unique=True,
        max_length=100,
        help_text=_("A short, descriptive name of a favorite thing category."),
    )
    created_date = models.DateTimeField(verbose_name=_("Created"), auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name=_("Modified"), auto_now=True)
    history = HistoricalRecords()

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name


class Thing(models.Model):
    objects = ThingManager()

    title = models.CharField(
        verbose_name=_("Title"),
        unique=True,
        max_length=100,
        help_text=_("A short, descriptive name of a favorite thing."),
    )
    description = models.TextField(
        verbose_name=_("Description"),
        blank=True,
        default="",
        validators=[MinLengthValidator(10)],
        help_text=_("Describe a favorite thing"),
    )
    ranking = models.IntegerField(
        verbose_name=_("Ranking"),
        help_text=_(
            "A numbered priority according to the user (1...n). Ranking numbers should not repeat for the given "
            "category, so if a ranking is set on a new favorite thing as '1', then all other favorite things for that "
            "category will be reordered."
        ),
    )
    metadata = JSONField(
        verbose_name=_("Metadata"),
        null=True,
        default=dict,
        help_text=_(
            format_lazy(
                """
                A key/value store where valid data types include text, number, date, and enum types. Users can enter 0
                or more keys with values attached. Click <a href="{}">here</a> for more information.
                """,
                reverse_lazy("help-metadata"),
            )
        ),
    )
    category = models.ForeignKey(
        Category,
        verbose_name=_("Category"),
        related_name="category",
        on_delete=models.CASCADE,
        help_text=_("Category of a favorite thing"),
    )
    created_date = models.DateTimeField(verbose_name=_("Created"), auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name=_("Modified"), auto_now=True)
    history = HistoricalRecords()

    class Meta:
        verbose_name_plural = "Things"
        ordering = ("category__name", "ranking")

    def __str__(self):
        return "%d: %s" % (self.category_id, self.title)
