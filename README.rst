Favorite Things
===============

My Favorite Things

.. image:: https://gitlab.com/kiawin/favorite-things/badges/master/pipeline.svg
     :target: https://gitlab.com/kiawin/favorite-things/commits/master
     :alt: CI Pipeline
.. image:: https://gitlab.com/kiawin/favorite-things/badges/master/coverage.svg
     :target: https://gitlab.com/kiawin/favorite-things/commits/master
     :alt: Coverage Report
.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
     :target: https://github.com/ambv/black
     :alt: Black code style


:License: GPLv3


Documentation
-------------

For API documentation, refer to http://localhost:8000/docs/ or https://things.kiawin.com/docs/

For metadata structure, refer to http://localhost:8000/help/metadata or http://things.kiawin.com/help/metadata

Entity Relationship Diagram
---------------------------

* Refer to favorites_ for more details

.. _favorites: https://gitlab.com/kiawin/favorite-things/tree/master/favorite_things/favorites

.. image:: https://gitlab.com/kiawin/favorite-things/raw/master/entity_relationship_diagram.png
     :alt: ERD

Basic Commands
--------------

Start Containers
^^^^^^^^^^^^^^^^

* To start the containers, use this command::

    $ docker-compose -f local.yml up --build


Run Tests
^^^^^^^^^

* To run tests, use this command::

    $ docker-compose -f local.yml run --rm django pytest --cov



Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^

* To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

* To create an **superuser account**, use this command::

    $ docker-compose -f local.yml run --rm django python manage.py createsuperuser

